import 'ol/ol.css';
import olms from 'ol-mapbox-style';
import Map from 'ol/Map';
import style from '../config/style.json';
import scanFiles from './scanfiles';
import mapChooser from './mapchooser';
import tileLoadFunction from './tileloadfunction';
import featureLoadFunction from './featureloadfunction';
import { setRoot } from './helpers';

let container;

const map = new Map({
  target: 'map',
});
map.getLayers().on('add', ({ element }) => {
  const source = element.getSource();
  const mapboxSource = element.get('mapbox-source');
  if (
    'setTileLoadFunction' in source &&
    source.getTileUrlFunction()([0, 0, 0]).startsWith('./')
  ) {
    source.setTileLoadFunction(tileLoadFunction(container, mapboxSource));
  } else if ('getFormat' in source && source.getUrl().startsWith('./')) {
    source.setLoader(featureLoadFunction(container, mapboxSource, source, map));
    source.once('change', () => {
      if (
        map.get('mapbox-style').center.toString() === style.center.toString()
      ) {
        map.getView().fit(source.getExtent());
      }
    });
  }
});

const dropzone = document.getElementById('dropzone');
dropzone.addEventListener('dragenter', (e) => e.preventDefault());
dropzone.addEventListener('dragover', (e) => e.preventDefault());
dropzone.addEventListener('drop', async (event) => {
  event.preventDefault();
  const items = event.dataTransfer.items;
  if (items.length > 1) {
    alert('Bitte nur 1 Datei oder Verzeichnis');
    return;
  }

  container = {};
  const item = items[0];
  const root =
    'getAsFileSystemHandle' in item
      ? await item.getAsFileSystemHandle()
      : item.webkitGetAsEntry();
  if (root) {
    setRoot(root);
    await scanFiles(root, container);
  }
  mapChooser(container, map);
});

const chooseDirectory = document.getElementById('boxtitle');
chooseDirectory.addEventListener('click', async () => {
  if (!('showDirectoryPicker' in window)) {
    alert('Sorry, in diesem Browser funktioniert nur drag/drop');
    return;
  }
  const dirHandle = await showDirectoryPicker();
  container = {};
  setRoot(dirHandle);
  await scanFiles(dirHandle, container);
  mapChooser(container, map);
});

olms(map, style);
