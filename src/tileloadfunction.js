import TileState from 'ol/TileState';
import { getDirFromDir, getFile, getFileFromDir } from './helpers';
import readMetadata from './readmetadata';

export default function tileLoadFunction(container, mapboxSource, format) {
  const key = Object.keys(container).find(
    (key) => container[key][0].name === mapboxSource
  );
  const fileSystemEntry = container[key][0];
  return async function (tile, path) {
    const [z, x, y] = path.split('/').slice(-3);
    try {
      const yDir = await getDirFromDir(fileSystemEntry, z);
      const xDir = await getDirFromDir(yDir, x);
      const file = await getFileFromDir(xDir, y);
      const arrayBufferView = new Uint8Array(await file.arrayBuffer());
      const blob = new Blob([arrayBufferView]);
      const url = URL.createObjectURL(blob);
      const image = tile.getImage();
      image.addEventListener('load', () => URL.revokeObjectURL(url));
      image.src = url;
    } catch (e) {
      tile.setState(TileState.ERROR);
    }
  };
}
