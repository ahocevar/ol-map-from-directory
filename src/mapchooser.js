import generateStyle from './generatestyle';
import download from './download';
import olms from 'ol-mapbox-style';
import View from 'ol/View';

export default function mapChooser(container, map) {
  const listing = document.getElementById('listing');
  const downloadIcon = document.getElementById('download');
  const mapIcon = document.getElementById('map-outline');

  listing.textContent = '';

  Object.keys(container)
    .sort()
    .forEach((key) => {
      const li = document.createElement('li');
      li.innerHTML = `<img src="${mapIcon.src}"> <img src="${downloadIcon.src}"> ${key}`;

      const showMapAction = li.querySelector(`img[src="${mapIcon.src}"]`);
      showMapAction.addEventListener('click', async () => {
        const styleJson = await generateStyle(container[key], key, map);
        map.getLayers().clear();
        map.setView(new View());
        olms(map, styleJson);
      });

      const downloadAction = li.querySelector(`img[src="${downloadIcon.src}"]`);
      downloadAction.addEventListener('click', async () => {
        const styleJson = await generateStyle(container[key], key, map);
        download(`${key}.style.json`, styleJson);
      });

      listing.appendChild(li);
    });
}
