import { setUserProjection } from 'ol/proj';

let root;
export function setRoot(dir) {
  if (isDirectory(dir)) {
    root = dir;
  }
}

export function isDirectory(entry) {
  return entry.isDirectory || entry.kind === 'directory';
}

export function isFile(entry) {
  return entry.isFile || entry.kind === 'file';
}

export async function getFile(fileEntry) {
  if (
    'FileSystemFileHandle' in window &&
    fileEntry instanceof FileSystemFileHandle
  ) {
    return await fileEntry.getFile();
  } else {
    return new Promise((resolve, reject) => {
      fileEntry.file(
        async (file) => resolve(file),
        (e) => reject(e)
      );
    });
  }
}

export async function getFileFromDir(dirEntry, name) {
  if (
    'FileSystemDirectoryHandle' in window &&
    dirEntry instanceof FileSystemDirectoryHandle
  ) {
    const fileHandle = await dirEntry.getFileHandle(name);
    return await getFile(fileHandle);
  } else {
    return new Promise((resolve, reject) => {
      dirEntry.getFile(
        name,
        {},
        async (fileEntry) => resolve(await getFile(fileEntry)),
        (e) => reject(e)
      );
    });
  }
}

export async function getDirFromDir(dirEntry, name) {
  if (
    'FileSystemDirectoryHandle' in window &&
    dirEntry instanceof FileSystemDirectoryHandle
  ) {
    return await dirEntry.getDirectoryHandle(name);
  } else {
    return new Promise((resolve, reject) => {
      dirEntry.getDirectory(
        name,
        {},
        (directoryEntry) => resolve(directoryEntry),
        (e) => reject(e)
      );
    });
  }
}

export async function getPath(entry) {
  if ('FileSystemHandle' in window && entry instanceof FileSystemHandle) {
    const parts = root && [root.name, ...(await root.resolve(entry))];
    return parts && parts.length ? `/${parts.join('/')}` : '';
  } else {
    return entry.fullPath;
  }
}
