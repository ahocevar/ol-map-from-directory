import { getFile } from './helpers';

export default function featureLoadFunction(
  container,
  mapboxSource,
  source,
  map
) {
  const key = Object.keys(container).find((key) =>
    container[key].find((entry) => entry.name === mapboxSource)
  );
  const fileSystemEntry = container[key].find(
    (entry) => entry.name === mapboxSource
  );
  return async function () {
    const file = await getFile(fileSystemEntry);
    const features = source.getFormat().readFeatures(await file.text(), {
      featureProjection: map.getView().getProjection(),
    });
    source.addFeatures(features);
  };
}
