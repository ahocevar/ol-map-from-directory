import { getCenter } from 'ol/extent';
import { transformExtent } from 'ol/proj';
import { getPath, isDirectory, isFile } from './helpers';
import readMetadata from './readmetadata';
import baseStyle from '../config/style.json';

export default async function generateStyle(fileSystemEntries, name, map) {
  let metadata = {};
  if (isDirectory(fileSystemEntries[0])) {
    metadata = await readMetadata(fileSystemEntries[0]);
  }
  const { minzoom, maxzoom, bounds, format } = metadata;

  return {
    ...baseStyle,
    name,
    ...(bounds && {
      center: getCenter(bounds),
      zoom: map
        .getView()
        .getZoomForResolution(
          map
            .getView()
            .getResolutionForExtent(
              transformExtent(
                bounds,
                'EPSG:4326',
                map.getView().getProjection()
              )
            )
        ),
    }),
    sources: {
      ...baseStyle.sources,
      ...(await fileSystemEntries.reduce(async (accPromise, entry) => {
        const acc = await accPromise;
        const fullPath = await getPath(entry);
        if (isFile(entry)) {
          acc[entry.name] = {
            type: 'geojson',
            data: `.${fullPath}`,
          };
        } else if (isDirectory(entry)) {
          acc[entry.name] = {
            type: 'raster',
            tileSize: 256,
            minzoom,
            maxzoom,
            tiles: [`.${fullPath}/{z}/{x}/{-y}.${format}`],
          };
        }
        return acc;
      }, Promise.resolve({}))),
    },
    layers: [
      ...baseStyle.layers,
      ...fileSystemEntries.map((entry) =>
        entry.name.startsWith('DOP')
          ? {
              id: entry.name,
              source: entry.name,
              type: 'raster',
            }
          : {
              id: entry.name,
              source: entry.name,
              type: 'fill',
              paint: {
                'fill-color': entry.name.startsWith('FS')
                  ? '#BADA55'
                  : '#014E7E',
                'fill-opacity': 0.5,
              },
            }
      ),
      ...fileSystemEntries.reduce((acc, entry) => {
        if (/^(FS|SL)_.*$/.test(entry.name)) {
          acc.push({
            id: `${entry.name}_point`,
            source: entry.name,
            type: 'circle',
            paint: {
              'circle-color': entry.name.startsWith('FS')
                ? '#BADA55'
                : '#014E7E',
              'circle-opacity': 0.5,
              'circle-radius': 10,
            },
          });
        }
        return acc;
      }, []),
    ],
  };
}
