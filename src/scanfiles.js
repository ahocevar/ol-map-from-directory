import { isDirectory } from './helpers';

export default async function scanFiles(item, container) {
  let type;
  if (/^(FS|SL|DOP)_.*$/.test(item.name)) {
    const parts = item.name.replace('.json', '').split('_');
    type = parts.shift();
    const key = parts.join('_');
    if (!(key in container)) {
      container[key] = [];
    }
    if (type === 'DOP') {
      container[key].unshift(item);
    } else {
      container[key].push(item);
    }
  }

  if (isDirectory(item)) {
    if (type === 'DOP') {
      // directory is a tiles-z directory
      return;
    }
    if (
      'FileSystemDirectoryHandle' in window &&
      item instanceof FileSystemDirectoryHandle
    ) {
      for await (const entry of item.values()) {
        if (isDirectory(entry)) {
          const directory = await item.getDirectoryHandle(entry.name);
          await scanFiles(directory, container);
        } else {
          const file = await item.getFileHandle(entry.name);
          await scanFiles(file, container);
        }
      }
    } else {
      let directoryReader = item.createReader();
      let continueReading = true;
      async function readEntries() {
        await new Promise((resolve, reject) => {
          directoryReader.readEntries(async function (entries) {
            continueReading = entries.length > 0;
            await Promise.all(
              entries.map((entry) => scanFiles(entry, container))
            );
            if (continueReading) {
              await readEntries();
            }
            resolve();
          });
        });
      }
      await readEntries();
    }
  }
}
