import { getFileFromDir } from './helpers';

export default async function readMetadata(fileSystemEntry) {
  const file = await getFileFromDir(fileSystemEntry, 'metadata.json');
  const metadata = JSON.parse(await file.text());
  const format = metadata.format;
  const minzoom = Number(metadata.minzoom);
  const maxzoom = Number(metadata.maxzoom);
  const bounds = metadata.bounds.split(',').map(Number);
  return { bounds, format, minzoom, maxzoom };
}
