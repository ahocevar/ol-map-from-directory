# OpenLayers map from directory

## Developing and building

    npm install

to install dependencies.

    npm start

to run the development server.

    npm run build

to create a production build in the `dist/` folder. If the build does not work, delete the `.parcel-cache` and `dist/` folders before building. 

    npm test

for basic syntax checks.

## Description of the application

Demo application to load tiles and GeoJSON vectors from the local file system.

Supports drag/drop and directory picker to load a directory.

The directory can contain GeoJSON files (with `.json` extension) and a tile directories. GeoJSON files are expected to start with `SF_`or `SL_`, tile folder names to start with `DOP_`. Inside a tile directory, a `metadata.json` file and a TMS folder structure with `z` folders containing `x` folders containing `y.${format}` files. The `format` is a property in `metadata.json`, among others. Here is an example:
```js
{
    "format": "png",
    "bounds": "16.463946010,47.063659029,16.469398941,47.065396556",
    "maxzoom": "19",
    "minzoom": "0"
}
```
GeoJSON files and tile folders that share the same name (except for the `SF_`, `SL_` or `DOP_` prefix and the `.json` extension) will be grouped into a single map.

When a directory is loaded, all available maps are listed below the drop zone. Clicking the map icon will open the map, clicking the download icon will download a [Mapbox Style](https://docs.mapbox.com/mapbox-gl-js/style-spec/) JSON file.